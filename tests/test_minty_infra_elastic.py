# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from elasticsearch import Elasticsearch
from minty_infra_elastic import ElasticsearchInfrastructure
from unittest import mock


class TestElasticsearchInfrastructure:
    def test___init__(self):
        infra = ElasticsearchInfrastructure("some_name")
        assert infra._es_instance is None
        assert infra._config_name == "some_name"

    def test___call__(self):
        infra = ElasticsearchInfrastructure("some_name")

        with mock.patch.object(
            infra, "_connect_to_es", autospec=True
        ) as connect:
            connect.return_value = "ES"
            es = infra(config={})

        assert es == "ES"
        assert infra._es_instance == es

    def test__connect_to_es(self):
        infra = ElasticsearchInfrastructure("some_name")

        with pytest.raises(KeyError):
            infra._connect_to_es({})

        es = infra._connect_to_es(config={"elasticsearch": {"some_name": {}}})
        assert isinstance(es, Elasticsearch)
